@extends('frontend.index')

@section('title')

@section('styles')

@section('content')
<!--Navbar End-->
<div class="page-title-area bg-1">
	<div class="container">
		<div class="page-title-content">
			<h2>Applications
			</h2>

			<ul>
				<li>
					<a href="index.html">
						Home
					</a>
				</li>

				<li class="active">Applications
			</ul>
		</div>
	</div>
</div>


<!-- Start Services Area -->
<section class="services-area bg-color pt-100 pb-70">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="site-heading">
					<h4>What we do</h4>
					<h2>What Applications We Offer</h2>
					<span class="heading_overlay"></span>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-lg-4 col-md-6">
				<div class="single-services-box-wrap">
					<div class="single-services box card-bg top-content">
						<img src="{{asset('frontend/assets/img/txtreader.png')}}" alt="">
						<h3>Text Reading
						</h3>
						<p>Amal glass read text such as documents, books, screens and paintings in Arabic and English
						</p>
					</div>

					<div class="single-services box card-bg bg-1 bottom-content">
						<h3>Text Reading
						</h3>
						<p>Amal glass read text such as documents, books, screens and paintings in Arabic and English
						</p>
					</div>
				</div>
			</div>

			<div class="col-lg-4 col-md-6">
				<div class="single-services-box-wrap">
					<div class="single-services box card-bg top-content">
						<img src="{{asset('frontend/assets/img/coins.png')}}" alt="">
						<h3>
							Currency recognition
						</h3>
						<p>It enables you to identify more than 100 types of banknotes</p>

					</div>

					<div class="single-services box card-bg bg-1 bottom-content">
						<h3>
							Currency recognition
						</h3>
						<p>It enables you to identify more than 100 types of banknotes</p>
					</div>
				</div>
			</div>

			<div class="col-lg-4 col-md-6">
				<div class="single-services-box-wrap">
					<div class="single-services box card-bg top-content">
						<img src="{{asset('frontend/assets/img/environment.png')}}" alt="">
						<h3>
							Know what's around you
						</h3>
						<p>Recognize and describe objects such as: electrical appliances, furniture, cars, fruits, etc
						</p>
					</div>

					<div class="single-services box card-bg bg-1 bottom-content">
						<h3>
							Know what's around you
						</h3>
						<p>Recognize and describe objects such as: electrical appliances, furniture, cars, fruits, etc
						</p>
					</div>
				</div>
			</div>

			<div class="col-lg-4 col-md-6">
				<div class="single-services-box-wrap">
					<div class="single-services box card-bg top-content">
						<img src="{{asset('frontend/assets/img/help.png')}}" alt="">
						<h3>
							Help me
						</h3>
						<p>It enables you to send a text message containing your location or a phone call to three
							pre-recorded contacts </p>
					</div>

					<div class="single-services box card-bg bg-1 bottom-content">
						<h3>
							Help me
						</h3>
						<p>It enables you to send a text message containing your location or a phone call to three
							pre-recorded contacts </p>

					</div>
				</div>
			</div>

			<div class="col-lg-4 col-md-6">
				<div class="single-services-box-wrap">
					<div class="single-services box card-bg top-content">
						<img src="{{asset('frontend/assets/img/icon.png')}}" alt="">
						<h3>The translator</h3>
						<p>You can read texts in more than 50 languages such as French, English, Chinese, Korean and
							Hindi, and then translate them into the user's preferred language </p>
					</div>

					<div class="single-services box card-bg bg-1 bottom-content">
						<h3>The translator</h3>
						<p>You can read texts in more than 50 languages such as French, English, Chinese, Korean and
							Hindi, and then translate them into the user's preferred language </p>
					</div>
				</div>
			</div>

			<div class="col-lg-4 col-md-6">
				<div class="single-services-box-wrap">
					<div class="single-services box card-bg top-content">
						<img src="{{asset('frontend/assets/img/dark.png')}}" alt="">
						<h3>
							Dark area warning
						</h3>
						<p>It warns the user by voice if he is in a dark area and automatically turns on the lamp to
							avoid danger</p>
					</div>

					<div class="single-services box card-bg bg-1 bottom-content">
						<h3>
							Dark area warning
						</h3>
						<p>It warns the user by voice if he is in a dark area and automatically turns on the lamp to
							avoid danger</p>
					</div>
				</div>
			</div>

			<div class="col-lg-4 col-md-6">
				<div class="single-services-box-wrap">
					<div class="single-services box card-bg top-content">
						<img src="{{asset('frontend/assets/img/player.png')}}" alt="">
						<h3>
							Voice notes recorder and player
						</h3>
						<p>Record voice notes and play them when you need them, such as shopping list, study or personal
							notes, and to-do list</p>
					</div>
					<h3>
						Voice notes recorder and player
					</h3>
					<p>Record voice notes and play them when you need them, such as shopping list, study or personal
						notes, and to-do list</p>
					<div class="single-services box card-bg bg-1 bottom-content">


					</div>
				</div>
			</div>

			<div class="col-lg-4 col-md-6">
				<div class="single-services-box-wrap">
					<div class="single-services box card-bg top-content">
						<img src="{{asset('frontend/assets/img/translator.png')}}" alt="">
						<h3>
							The clock
						</h3>
						<p>No need for a Braille or talking watch. Amal glass tell you the time, day and date, plus a
							built-in stopwatch</p>
					</div>

					<div class="single-services box card-bg bg-1 bottom-content">
						<h3>
							The clock
						</h3>
						<p>No need for a Braille or talking watch. Amal glass tell you the time, day and date, plus a
							built-in stopwatch</p>

					</div>
				</div>
			</div>

			<div class="col-lg-4 col-md-6">
				<div class="single-services-box-wrap">
					<div class="single-services box card-bg top-content">
						<img src="{{asset('frontend/assets/img/light.png')}}" alt="">
						<h3>
							Flashlight
						</h3>
						<p>It enables you to light up dark places during an emergency or when needed
						</p>
					</div>

					<div class="single-services box card-bg bg-1 bottom-content">
						<h3>
							Flashlight
						</h3>
						<p>
							It enables you to light up dark places during an emergency or when needed
						</p>

					</div>
				</div>
			</div>

			<div class="col-lg-4 col-md-6">
				<div class="single-services-box-wrap">
					<div class="single-services box card-bg top-content">
						<img src="{{asset('frontend/assets/img/qiblah.png')}}" alt="">
						<h3>
							Qibla
						</h3>
						<p>It guides you in the direction of the qiblah to perform the prayer
						</p>
					</div>

					<div class="single-services box card-bg bg-1 bottom-content">
						<h3>
							Qibla
						</h3>
						<p>It guides you in the direction of the qiblah to perform the prayer
						</p>
					</div>
				</div>
			</div>

			<div class="col-lg-4 col-md-6">
				<div class="single-services-box-wrap">
					<div class="single-services box card-bg top-content">
						<img src="{{asset('frontend/assets/img/head.png')}}" alt="">
						<h3>
							Head position setting
						</h3>
						<p>Gives an audible alert to the user in case the head is placed in a position that hurts the
							vertebrae of the neck or spine
						</p>
					</div>

					<div class="single-services box card-bg bg-1 bottom-content">
						<h3>
							Head position setting
						</h3>
						<p>Gives an audible alert to the user in case the head is placed in a position that hurts the
							vertebrae of the neck or spine
						</p>

					</div>
				</div>
			</div>

			<div class="col-lg-4 col-md-6">
				<div class="single-services-box-wrap">
					<div class="single-services box card-bg top-content">
						<img src="{{asset('frontend/assets/img/wether.png')}}" alt="">
						<h3>
							Weather
						</h3>
						<p>It shows the user from knowing the weather today, such as temperature, wind speed, and the
							possibility of rain </p>
					</div>

					<div class="single-services box card-bg bg-1 bottom-content">
						<h3>
							Weather
						</h3>
						<p>It shows the user from knowing the weather today, such as temperature, wind speed, and the
							possibility of rain
						</p>

					</div>
				</div>
			</div>

			<div class="col-lg-4 col-md-6">
				<div class="single-services-box-wrap">
					<div class="single-services box card-bg top-content">
						<img src="{{asset('frontend/assets/img/location.png')}}" alt="">
						<h3>
							Address
						</h3>
						<p>Enable the user to know his current address (such as street name, neighborhood name and city)
							in addition to registering it and the possibility of sending it to the previously registered
							contact
						</p>
					</div>

					<div class="single-services box card-bg bg-1 bottom-content">
						<h3>
							Address
						</h3>
						<p>Enable the user to know his current address (such as street name, neighborhood name and city)
							in addition to registering it and the possibility of sending it to the previously registered
							contact
						</p>
					</div>
				</div>
			</div>

			<div class="col-lg-4 col-md-6">
				<div class="single-services-box-wrap">
					<div class="single-services box card-bg top-content">
						<img src="{{asset('frontend/assets/img/control.png')}}" alt="">
						<h3>TV remote control </h3>
						<p>It enables you to turn the TV on and off, lower the volume, raise the volume, and mute the
							volume as needed</p>
					</div>

					<div class="single-services box card-bg bg-1 bottom-content">
						<h3>TV remote control </h3>
						<p>It enables you to turn the TV on and off, lower the volume, raise the volume, and mute the
							volume as needed</p>

					</div>
				</div>
			</div>

			<div class="col-lg-4 col-md-6">
				<div class="single-services-box-wrap">
					<div class="single-services box card-bg top-content">
						<img src="{{asset('frontend/assets/img/barcode.png')}}" alt="">
						<h3>
							Barcode QR Code Reader
						</h3>
						<p>The user can identify products that contain barcodes within the predefined database according
							to the user's country</p>
					</div>

					<div class="single-services box card-bg bg-1 bottom-content">
						<h3>
							Barcode QR Code Reader
						</h3>
						<p>The user can identify products that contain barcodes within the predefined database according
							to the user's country</p>
					</div>

				</div>
			</div>

			<div class="col-lg-4 col-md-6">
				<div class="single-services-box-wrap">
					<div class="single-services box card-bg top-content">
						<img src="{{asset('frontend/assets/img/icon.png')}}" alt="">
						<h3>
							System language settings
						</h3>
						<p>Enables the user to choose the preferred operating system language as well as change the
							reader speed or choose the default reading speed</p>
					</div>

					<div class="single-services box card-bg bg-1 bottom-content">
						<h3>
							System language settings
						</h3>
						<p>Enables the user to choose the preferred operating system language as well as change the
							reader speed or choose the default reading speed</p>
					</div>
				</div>
			</div>

		</div>

	</div>


</section>
@endsection

@section('scripts')