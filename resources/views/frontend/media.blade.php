@extends('frontend.index')

@section('title')

@section('styles')

@section('content')

<div class="page-title-area bg-1">
	<div class="container">
		<div class="page-title-content">
			<h2>Media Center
			</h2>

			<ul>
				<li>
					<a href="{{route('front.welcome')}}">
						Home
					</a>
				</li>

				<li class="active">Media Center
				</li>
			</ul>
		</div>
	</div>
</div>
<!-- Star Media Center \\\\\\-->
<section class="blog-page-area ptb-100">
	<div class="container">
		<div class="row">
			<div class="col-lg-6">
				<div class="row">
					<div class="col-lg-12 col-md-6">
						<div class="single-blog">
							<a href="media_center1.html">
								<img src="{{asset('frontend/assets/img/Bolg-2.jpg')}}" alt="Image">
							</a>
							<span>
								<i class="ri-calendar-line"></i>
								29 January, 2022
							</span>
							<h3>
								<a href="media_center1.html">
									«HOPE» GLASSES THAT ALLOW THE BLIND TO IDENTIFY THE SURROUNDING THINGS
									THROUGH A VIRTUAL ASSISTANT
								</a>
							</h3>
							<p>Du, a subsidiary of Emirates Integrated Telecommunications Company (EITC), today
								announced its collaboration with Amal Glass to showcase the benefits and smart
								solutions provided by Amel Glasses to the blind and visually impaired in the UAE
								at GITEX Technology Week 2018. "Du said, Du, a subsidiary of Emirates Integrated
								Telecommunications Company (EITC), today announced its collaboration with A ...
							</p>


						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-6">
				<div class="row">
					<div class="col-lg-12 col-md-6">
						<div class="single-blog">
							<a href="media_center2.html">
								<img src="{{asset('frontend/assets/img/Bolg-1.jpg')}}" alt="Image">
							</a>
							<span>
								<i class="ri-calendar-line"></i>
								29 January, 2022
							</span>
							<h3>
								<a href="media_center2.html">
									10 START-UPS THAT ARE HELPING TO CHANGE THE ARAB WORLD

								</a>
							</h3>
							<p>Entrepreneurship is booming across the Middle East and North Africa. In 2018,
								$900 million was invested across the region in 386 deals, an increase of 31% in
								total funding in 2017, according to a report released ahead of the World
								Economic Forum on the Middle East and North Africa, which is taking place in
								Jordan on 6-7 April. Image: Magnitt The report, The Start-up Ecosyst ...</p>


						</div>
					</div>
				</div>
			</div>

			<div class="col-lg-6">
				<div class="row">
					<div class="col-lg-12 col-md-6">
						<div class="single-blog">
							<a href="media_center3.html">
								<img src="{{asset('frontend/assets/img/Bolg-3.jpg')}}" alt="Image">
							</a>
							<span>
								<i class="ri-calendar-line"></i>
								29 January, 2022
							</span>
							<h3>
								<a href="media_center3.html">
									«HOPE» GLASSES THAT ALLOW THE BLIND TO IDENTIFY THE SURROUNDING THINGS
									THROUGH A VIRTUAL ASSISTANT
								</a>
							</h3>
							<p>Du, a subsidiary of Emirates Integrated Telecommunications Company (EITC), today
								announced its collaboration with Amal Glass to showcase the benefits and smart
								solutions provided by Amel Glasses to the blind and visually impaired in the UAE
								at GITEX Technology Week 2018. "Du said, Du, a subsidiary of Emirates Integrated
								Telecommunications Company (EITC), today announced its collaboration with A ...
							</p>


						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-6">
				<div class="row">
					<div class="col-lg-12 col-md-6">
						<div class="single-blog">
							<a href="media_center4.html">
								<img src="{{asset('frontend/assets/img/Bolg-4.jpg')}}" alt="Image">
							</a>
							<span>
								<i class="ri-calendar-line"></i>
								29 January, 2022
							</span>
							<h3>
								<a href="media_center4.html">
									10 START-UPS THAT ARE HELPING TO CHANGE THE ARAB WORLD

								</a>
							</h3>
							<p>Entrepreneurship is booming across the Middle East and North Africa. In 2018,
								$900 million was invested across the region in 386 deals, an increase of 31% in
								total funding in 2017, according to a report released ahead of the World
								Economic Forum on the Middle East and North Africa, which is taking place in
								Jordan on 6-7 April. Image: Magnitt The report, The Start-up Ecosyst ...</p>


						</div>
					</div>
				</div>
			</div>

			<div class="col-lg-6">
				<div class="row">
					<div class="col-lg-12 col-md-6">
						<div class="single-blog">
							<a href="media_center5.html">
								<img src="{{asset('frontend/assets/img/Bolg-5.jpg')}}" alt="Image">
							</a>
							<span>
								<i class="ri-calendar-line"></i>
								29 January, 2022
							</span>
							<h3>
								<a href="media_center5.html">
									«HOPE» GLASSES THAT ALLOW THE BLIND TO IDENTIFY THE SURROUNDING THINGS
									THROUGH A VIRTUAL ASSISTANT
								</a>
							</h3>
							<p>Du, a subsidiary of Emirates Integrated Telecommunications Company (EITC), today
								announced its collaboration with Amal Glass to showcase the benefits and smart
								solutions provided by Amel Glasses to the blind and visually impaired in the UAE
								at GITEX Technology Week 2018. "Du said, Du, a subsidiary of Emirates Integrated
								Telecommunications Company (EITC), today announced its collaboration with A ...
							</p>


						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-6">
				<div class="row">
					<div class="col-lg-12 col-md-6">
						<div class="single-blog">
							<a href="media_center6.html">
								<img src="{{asset('frontend/assets/img/Bolg-6.jpg')}}" alt="Image">
							</a>
							<span>
								<i class="ri-calendar-line"></i>
								29 January, 2022
							</span>
							<h3>
								<a href="media_center6.html">
									10 START-UPS THAT ARE HELPING TO CHANGE THE ARAB WORLD

								</a>
							</h3>
							<p>Entrepreneurship is booming across the Middle East and North Africa. In 2018,
								$900 million was invested across the region in 386 deals, an increase of 31% in
								total funding in 2017, according to a report released ahead of the World
								Economic Forum on the Middle East and North Africa, which is taking place in
								Jordan on 6-7 April. Image: Magnitt The report, The Start-up Ecosyst ...</p>


						</div>
					</div>
				</div>
			</div>
		</div>

		<!-- <div class="row">
                        <div class="pagination-area">
                            <span class="page-numbers current" aria-current="page">1</span>
                            <a href="#" class="page-numbers">2</a>
                            <a href="#" class="page-numbers">3</a>
                            
                            <a href="#" class="next page-numbers">
                                <i class="fa fa-arrow-right" style="position: absolute; top: 10;right: 10;
                            "></i>
                            </a>
                        </div>
                    </div> -->

	</div>


	</div>
	</div>
</section>
<!-- end Media Center  page \\\\\\-->
@endsection

@section('scripts')