@extends('frontend.index')

@section('title')

@section('styles')

@section('content')
<div class="page-title-area bg-1">
		<div class="container">
			<div class="page-title-content">
				<h2>About</h2>
	
				<ul>
					<li>
						<a href="index.html">
							Home
						</a>
					</li>
	
					<li class="active">About</li>
				</ul>
			</div>
		</div>
	</div>
	
	<!-- Start About Us Area -->
	<section class="about-us-area  pt-100 pb-70">
		<div class="container">
			<div class="row align-items-center">
				<div class="col-lg-5">
					<div class="about-img-three before-style">
						<img src="{{asset('frontend/assets/img/about.png')}}" alt="Image">
					</div>
				</div>
	
				<div class="col-lg-7">
					<div class="about-content about-content-style-two mb-0">
						<div class="row">
							<div class="col-lg-12 col-sm-12">
								<div class="site-heading wow fadeInUp" data-wow-duration="2s" data-wow-delay="0.3s"
									style="visibility: hidden; animation-duration: 2s; animation-delay: 0.3s; animation-name: none; margin: 0;">
	
									<h2 style="text-align: left;">Our Start</h2>
									<span class="heading_overlay"></span>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-lg-12 col-sm-12">
								<p>
									The idea started with the creation of an intelligent robot that can identify the
									environment and movement, which was the field of study and specialization of Engineer
									Mohammed Islam.
									Then the idea turned from spending effort and time in making the robot to making this
									robot serve humanity and harness all its capabilities to be a support for the human
									being. After many research and development, work began to transform this robot since
									2010 into smart glasses for the blind.
									According to the reports of the World Health Organization, the number of blind people
									has reached more than 60 million, and the number of visually impaired people has reached
									250 million. Hence the idea came to invent a smart technology that enables them to
									coexist naturally and to be self-reliant and not to ask for help from others.
	
	
	
								</p>
							</div>
	
	
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	
	<section class="about-us-area  pt-100 pb-70">
		<div class="container">
			<div class="row align-items-center">
				<div class="col-lg-7">
					<div class="about-content about-content-style-two mb-0">
						<div class="row">
							<div class="col-lg-12 col-sm-12">
								<div class="site-heading wow fadeInUp" data-wow-duration="2s" data-wow-delay="0.3s"
									style="visibility: hidden; animation-duration: 2s; animation-delay: 0.3s; animation-name: none; margin: 0;">
	
									<h2 style="text-align: left;">Definition of Amal glass</h2>
									<span class="heading_overlay"></span>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-lg-12 col-sm-12" style="margin-bottom:70px;">
								<p>
									The idea started with the creation of an intelligent robot that can identify the
									environment and movement without collision and sensing the obstacles around it, which
									was the field of study and specialization of Engineer Mohammed Islam. <br>
	
									And then, the idea turned from spending the effort and time in the manufacture of a
									robot to make this robot serves humanity and harness all of its potential to be a
									support to the human being rather than be a luxury and after many research, we found the
									most needy people are those with special needs. <br>
	
									Work has begun to turn this robots robot since 2010 into smart glasses that serve the
									blind. Hence the idea of Amal smart glasses began. After much research, we found that
									many blind people need a personal assistant and some may be isolated from the outside
									world. <br>
	
									We have been working in Amal glasses to harness all our potential to make these glasses
									after Allah is the personal attendant that can provide all the needs of the person who
									lost the grace of sight, Allah willing, without the need for anyone and help him with
									his needs and fully integrate into the society. <br>
	
								</p>
							</div>
	
	
						</div>
					</div>
				</div>
	
				<div class="col-lg-5">
					<div class="about-img-three before-style">
						<img src="{{asset('frontend/assets/img/about.png')}}" alt="Image">
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- End About Us Area -->
@endsection

@section('scripts')