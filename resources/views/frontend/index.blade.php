<html lang="en-US">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="description" content="amal | Product Landing Page from Themescare">
	<meta name="author" content="Themescare">
	<!-- Title -->
	<title>amal - Product Landing Page @yield('title')</title>
	<!-- Favicon -->
	<link rel="icon" type="image/png" sizes="32x32" href="{{asset('frontend/assets/img/favicon/favicon-32x32.png')}}">
	<!--Bootstrap css-->
	<link rel="stylesheet" href="{{asset('frontend/assets/css/bootstrap.css')}}">
	<!--Font Awesome css-->
	<link rel="stylesheet" href="{{asset('frontend/assets/css/font-awesome.min.css')}}">
	<!--Flaticon css-->
	<link rel="stylesheet" href="{{asset('frontend/assets/flaticon/flaticon.css')}}">
	<!--Magnific css-->
	<link rel="stylesheet" href="{{asset('frontend/assets/css/magnific-popup.css')}}">
	<!--Owl-Carousel css-->
	<link rel="stylesheet" href="{{asset('frontend/assets/css/owl.carousel.min.css')}}">
	<link rel="stylesheet" href="{{asset('frontend/assets/css/owl.theme.default.min.css')}}">
	<!--Animate css-->
	<link rel="stylesheet" href="{{asset('frontend/assets/css/animate.min.css')}}">
	<!--Site Main Style css-->
	<link rel="stylesheet" href="{{asset('frontend/assets/css/style.css')}}">
	<!--Responsive css-->
	<link rel="stylesheet" href="{{asset('frontend/assets/css/responsive.css')}}">
	@yield('styles')
</head>

<body>


	<!--Navbar Start-->
	<nav class="navbar navbar-expand-lg">
		<div class="container">
			<!-- LOGO -->
			<a class="navbar-brand logo" href="{{route('front.welcome')}}">
				<img src="{{asset('frontend/assets/img/logo.png')}}" alt="amal">
			</a>
			<a class="navbar-brand logo HideScroll" href="{{route('front.welcome')}}">
				<img src="{{asset('frontend/assets/img/logo.png')}}" alt="amal">
			</a>
			<button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarCollapse"
				aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
				<span class="fa fa-bars"></span>
			</button>
			<div class="navbar-collapse collapse" id="navbarCollapse">
				<ul class="navbar-nav ml-auto">
					<li class="nav-item">
						<a href="{{route('front.welcome')}}" class="nav-link active">Home</a>
					</li>
					<li class="nav-item">
						<a href="#" class="nav-link" data-scroll-nav="1">Applications</a>
					</li>
					<li class="nav-item">
						<a href="#" class="nav-link" data-scroll-nav="2">About</a>
					</li>

					<li class="nav-item">
						<a href="{{route('front.media')}}" class="nav-link">Media Center</a>
					</li>
					<li class="nav-item">
						<a href="#" class="nav-link" data-scroll-nav="4">Trophies</a>
					</li>
					<li class="nav-item">
						<a href="#" class="nav-link" data-scroll-nav="5">Store</a>
					</li>
					<li class="nav-item">
						<a href="#" class="nav-link" data-scroll-nav="6">contact</a>
					</li>
					<li class="nav-item dropdown">
						<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
							data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							En
						</a>
						<div class="dropdown-menu" aria-labelledby="navbarDropdown">

							<a class="dropdown-item" href="index-ar.html">عربي</a>

						</div>
					</li>
				</ul>
			</div>
		</div>
	</nav>
	<!--Navbar End-->


	@yield('content')

	<!-- Footer Area Start -->
	<footer class="amal-footer-area">
		<div class="ocean">
			<div class="wave"></div>
			<div class="wave"></div>
		</div>
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="footer-box">
						<div class="footer-logo wow fadeInUp" data-wow-duration="2s" data-wow-delay="0.3s"
							style="visibility: hidden; animation-duration: 2s; animation-delay: 0.3s; animation-name: none;">
							<a href="index.html"><img src="{{asset('frontend/assets/css/animate.min.css')}}" alt="footer logo"></a>
						</div>
						<div class="footer-copyright wow fadeInUp" data-wow-duration="2s" data-wow-delay="0.5s"
							style="visibility: hidden; animation-duration: 2s; animation-delay: 0.5s; animation-name: none;">
							<p>Copyrights © All Rights Reserved by Amal Glass</p>
						</div>

						<div class="xx">
							<ul>
								<li class="wow fadeInLeft animated" data-wow-duration="2s" data-wow-delay="0.3s"
									style="visibility: visible; animation-duration: 2s; animation-delay: 0.3s; animation-name: fadeInLeft;">
									<a href="https://www.facebook.com/AmalGlass/"><i class="fa fa-facebook"></i></a>
								</li>
								<li class="wow fadeInLeft animated" data-wow-duration="2s" data-wow-delay="0.5s"
									style="visibility: visible; animation-duration: 2s; animation-delay: 0.5s; animation-name: fadeInLeft;">
									<a href="https://twitter.com/amal_glass"><i class="fa fa-twitter"></i></a>
								</li>
								<li class="wow fadeInLeft animated" data-wow-duration="2s" data-wow-delay="0.3s"
									style="visibility: visible; animation-duration: 2s; animation-delay: 0.3s; animation-name: fadeInLeft;">
									<a href="https://www.instagram.com/amalglass/"><i class="fa fa-Instagram"></i></a>
								</li>

								<li class="wow fadeInLeft animated" data-wow-duration="2s" data-wow-delay="0.7s"
									style="visibility: visible; animation-duration: 2s; animation-delay: 0.7s; animation-name: fadeInLeft;">
									<a
										href="https://www.linkedin.com/authwall?trk=bf&trkInfo=AQFQX2BTbIZEhAAAAX6_aF143tBADH0QdLHkxac0EOsjDIpK0Xmkwita6EL3Ssh-YIS5q2h8C3zf4LjIa31QKnxZ9Pkn1QU19OsNrxhHbvA8pGrU_76EXNk5813g3tGQ8o-aV-0=&originalReferer=&sessionRedirect=https%3A%2F%2Fwww.linkedin.com%2Fcompany%2Famalglass%2F"><i
											class="fa fa-linkedin"></i></a>
								</li>
								<li class="wow fadeInLeft animated" data-wow-duration="2s" data-wow-delay="0.9s"
									style="visibility: visible; animation-duration: 2s; animation-delay: 0.9s; animation-name: fadeInLeft;">
									<a href="https://www.youtube.com/c/AmalGlass"><i class="fa fa-youtube"></i></a>
								</li>
							</ul>
						</div>

						<div class="terms ">
							<a href="Privact_policy.html">Terms of Use & Privacy Policy</a>
						</div>
					</div>

				</div>
			</div>
		</div>
	</footer>
	<!-- Footer Area End -->


	<!--Jquery js-->
	<script src="{{asset('frontend/assets/js/jquery.min.js')}}"></script>
	<!-- Popper JS -->
	<script src="{{asset('frontend/assets/js/popper.min.js')}}"></script>
	<!--Bootstrap js-->
	<script src="{{asset('frontend/assets/js/bootstrap.min.js')}}"></script>
	<!--ScrollIt js-->
	<script src="{{asset('frontend/assets/js/scrollIt.min.js')}}"></script>
	<!--Owl-Carousel js-->
	<script src="{{asset('frontend/assets/js/owl.carousel.min.js')}}"></script>
	<!--Magnific js-->
	<script src="{{asset('frontend/assets/js/jquery.magnific-popup.min.js')}}"></script>
	<!--Initial js-->
	<script src="{{asset('frontend/assets/js/init.js')}}"></script>
	<!--Wow js-->
	<script src="{{asset('frontend/assets/js/wow.min.js')}}"></script>
	<!--Main js-->
	<script src="{{asset('frontend/assets/js/main.js')}}"></script>

	<script type="text/javascript">
		var $zoho=$zoho || {};$zoho.salesiq = $zoho.salesiq || {widgetcode:"55efb9103e787365daf489039ec9e73f7be39f214304135c234590d81ac9870a", values:{},ready:function(){}};var d=document;s=d.createElement("script");s.type="text/javascript";s.id="zsiqscript";s.defer=true;s.src="https://salesiq.zoho.com/widget";t=d.getElementsByTagName("script")[0];t.parentNode.insertBefore(s,t);
	</script>

  @yield('scripts')

</body>

</html>