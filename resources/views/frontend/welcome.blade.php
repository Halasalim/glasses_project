@extends('frontend.index')

@section('title')

@section('styles')

@section('content')
<!-- Welcome Area Start -->
	<section class="amal-welcome-area fix" data-scroll-index="0">
		<!-- Shape Start -->
		<div class="shapes-container">
			<div class="bg-shape"></div>
		</div>
		<!-- Shape End -->
		<div class="container">
			<div class="row align-items-center">
				<div class="col-12 col-md-7">
					<div class="welcome-text">
						<h2 style="margin-bottom:20px;">Amal glass</h2>
						<h3 style="text-align:left;">Smart glasses for the blind and visually</h3>
						<h3 style="margin-bottom:10px; text-align:left;"> visually impaired</h3>
						<p>
							It resembles sunglasses, contains a camera and a set of sensors, and connects to a mobile
							phone, an Android system to enable the blind to be independent and self-reliant to practice
							their daily lives, integrate into education and the labor market, and not seek help from
							others.
						</p>
						<!-- <a href="#"  class="amal-btn">Buy Products<span></span></a> -->
					</div>
				</div>
				<div class="col-12 col-md-5">
					<div class="welcome-image">
						<img src="{{asset('frontend/assets/img/hero.png')}}" alt="welcome" class="animation-jump">
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Welcome Area End -->

	<!-- Applications Area Start -->
	<section class="amal-Applications-area" data-scroll-index="1">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="site-heading">
						<h4>Applications</h4>
						<h2>Amal Glass App</h2>
						<span class="heading_overlay"></span>
					</div>
				</div>
			</div>
	
			<div class="row">
				<div class="col-lg-12">
					<div class=" wow fadeInRight" data-wow-duration="2s" data-wow-delay="0.5s"
						style="visibility: hidden; animation-duration: 2s; animation-delay: 0.5s; animation-name: none;">
						<div class="featured-slider owl-carousel owl-loaded owl-drag">
							<div class="owl-stage-outer">
								<div class="owl-stage">
									<div class="owl-item  ">
										<div class="single-testimonial">
											<div class="single-feature wow fadeInLeft" data-wow-duration="2s"
												data-wow-delay="0.3s"
												style="visibility: hidden; animation-duration: 2s; animation-delay: 0.3s; animation-name: none;">
												<div class="feature-icon">
													<img src="{{asset('frontend/assets/img/txtreader.png')}}" alt="">
												</div>
												<div class="feature-text">
													<h3>Text Reading</h3>
													<p>Amal glass read text such as documents, books, screens and
														paintings in Arabic and English</p>
												</div>
											</div>
										</div>
									</div>
									<div class="owl-item  ">
										<div class="single-feature wow fadeInLeft" data-wow-duration="2s"
											data-wow-delay="0.3s"
											style="visibility: hidden; animation-duration: 2s; animation-delay: 0.3s; animation-name: none;">
											<div class="feature-icon">
												<img src="{{asset('frontend/assets/img/coins.png')}}" alt="">
											</div>
											<div class="feature-text">
												<h3>Currency recognition </h3>
												<p>It enables you to identify more than 100 types of banknotes</p>
											</div>
										</div>
									</div>
									<div class="owl-item  ">
										<div class="single-feature wow fadeInLeft" data-wow-duration="2s"
											data-wow-delay="0.3s"
											style="visibility: hidden; animation-duration: 2s; animation-delay: 0.3s; animation-name: none;">
											<div class="feature-icon">
												<img src="{{asset('frontend/assets/img/environment.png')}}" alt="">
											</div>
											<div class="feature-text">
												<h3>Know what's around you</h3>
												<p>Recognize and describe objects such as: electrical appliances,
													furniture, cars, fruits, etc</p>
											</div>
										</div>
									</div>
									<div class="owl-item  ">
										<div class="single-feature wow fadeInLeft" data-wow-duration="2s"
											data-wow-delay="0.3s"
											style="visibility: hidden; animation-duration: 2s; animation-delay: 0.3s; animation-name: none;">
											<div class="feature-icon">
												<img src="{{asset('frontend/assets/img/help.png')}}" alt="">
											</div>
											<div class="feature-text">
												<h3>Help me</h3>
												<p>
													It enables you to send a text message containing your location or a
													phone call to three pre-recorded contacts
												</p>
											</div>
										</div>
	
	
	
									</div>
									<div class="owl-item  ">
										<div class="single-testimonial">
											<div class="single-feature wow fadeInLeft" data-wow-duration="2s"
												data-wow-delay="0.3s"
												style="visibility: hidden; animation-duration: 2s; animation-delay: 0.3s; animation-name: none;">
												<div class="feature-icon">
													<img src="{{asset('frontend/assets/img/translator.png')}}" alt="">
												</div>
												<div class="feature-text">
													<h3>The translator </h3>
													<p>
														You can read texts in more than 50 languages such as French,
														English, Chinese, Korean and Hindi, and then translate them into
														the user's preferred language
													</p>
												</div>
											</div>
										</div>
									</div>
									<div class="owl-item  ">
										<div class="single-feature wow fadeInLeft" data-wow-duration="2s"
											data-wow-delay="0.3s"
											style="visibility: hidden; animation-duration: 2s; animation-delay: 0.3s; animation-name: none;">
											<div class="feature-icon">
												<img src="{{asset('frontend/assets/img/dark.png')}}" alt="">
											</div>
											<div class="feature-text">
												<h3>Dark area warning</h3>
												<p>It warns the user by voice if he is in a dark area and automatically
													turns on the lamp to avoid danger</p>
											</div>
										</div>
									</div>
									<div class="owl-item  ">
										<div class="single-feature wow fadeInLeft" data-wow-duration="2s"
											data-wow-delay="0.3s"
											style="visibility: hidden; animation-duration: 2s; animation-delay: 0.3s; animation-name: none;">
											<div class="feature-icon">
												<img src="{{asset('frontend/assets/img/player.png')}}" alt="">
											</div>
											<div class="feature-text">
												<h3> Voice notes recorder and player </h3>
												<p>Record voice notes and play them when you need them, such as shopping
													list, study or personal notes, and to-do list</p>
											</div>
										</div>
									</div>
									<div class="owl-item  ">
										<div class="single-feature wow fadeInLeft" data-wow-duration="2s"
											data-wow-delay="0.3s"
											style="visibility: hidden; animation-duration: 2s; animation-delay: 0.3s; animation-name: none;">
											<div class="feature-icon">
												<img src="{{asset('frontend/assets/img/watch.png')}}" alt="">
											</div>
											<div class="feature-text">
												<h3>The clock </h3>
												<p>No need for a Braille or talking watch. Amal glass tell you the time,
													day and date, plus a built-in stopwatch</p>
											</div>
										</div>
									</div>
									<div class="owl-item  ">
										<div class="single-feature wow fadeInLeft" data-wow-duration="2s"
											data-wow-delay="0.3s"
											style="visibility: hidden; animation-duration: 2s; animation-delay: 0.3s; animation-name: none;">
											<div class="feature-icon">
												<img src="{{asset('frontend/assets/img/light.png')}}" alt="">
											</div>
											<div class="feature-text">
												<h3>Flashlight</h3>
												<p>It enables you to light up dark places during an emergency or when
													needed</p>
											</div>
										</div>
									</div>
									<div class="owl-item  ">
										<div class="single-feature wow fadeInLeft" data-wow-duration="2s"
											data-wow-delay="0.3s"
											style="visibility: hidden; animation-duration: 2s; animation-delay: 0.3s; animation-name: none;">
											<div class="feature-icon">
												<img src="{{asset('frontend/assets/img/qiblah.png')}}" alt="">
											</div>
											<div class="feature-text">
												<h3>Qibla </h3>
												<p>It guides you in the direction of the qiblah to perform the prayer
												</p>
											</div>
										</div>
									</div>
									<div class="owl-item  ">
										<div class="single-feature wow fadeInLeft" data-wow-duration="2s"
											data-wow-delay="0.3s"
											style="visibility: hidden; animation-duration: 2s; animation-delay: 0.3s; animation-name: none;">
											<div class="feature-icon">
												<img src="{{asset('frontend/assets/img/head.png')}}" alt="">
											</div>
											<div class="feature-text">
												<h3>Head position setting </h3>
												<p>Gives an audible alert to the user in case the head is placed in a
													position that hurts the vertebrae of the neck or spine</p>
											</div>
										</div>
									</div>
									<div class="owl-item  ">
										<div class="single-feature wow fadeInLeft" data-wow-duration="2s"
											data-wow-delay="0.3s"
											style="visibility: hidden; animation-duration: 2s; animation-delay: 0.3s; animation-name: none;">
											<div class="feature-icon">
												<img src="{{asset('frontend/assets/img/wether.png')}}" alt="">
											</div>
											<div class="feature-text">
												<h3>Weather </h3>
												<p>It shows the user from knowing the weather today, such as
													temperature, wind speed, and the possibility of rain</p>
											</div>
										</div>
									</div>
									<div class="owl-item  ">
										<div class="single-feature wow fadeInLeft" data-wow-duration="2s"
											data-wow-delay="0.3s"
											style="visibility: hidden; animation-duration: 2s; animation-delay: 0.3s; animation-name: none;">
											<div class="feature-icon">
												<img src="{{asset('frontend/assets/img/location.png')}}" alt="">
											</div>
											<div class="feature-text">
												<h3>Address </h3>
												<p>Enable the user to know his current address (such as street name,
													neighborhood name and city) in addition to registering it and the
													possibility of sending it to the previously registered contact</p>
											</div>
										</div>
									</div>
									<div class="owl-item  ">
										<div class="single-feature wow fadeInLeft" data-wow-duration="2s"
											data-wow-delay="0.3s"
											style="visibility: hidden; animation-duration: 2s; animation-delay: 0.3s; animation-name: none;">
											<div class="feature-icon">
												<img src="{{asset('frontend/assets/img/control.png')}}" alt="">
											</div>
											<div class="feature-text">
												<h3>TV remote control </h3>
												<p>It enables you to turn the TV on and off, lower the volume, raise the
													volume, and mute the volume as needed</p>
											</div>
										</div>
									</div>
									<div class="owl-item  ">
										<div class="single-feature wow fadeInLeft" data-wow-duration="2s"
											data-wow-delay="0.3s"
											style="visibility: hidden; animation-duration: 2s; animation-delay: 0.3s; animation-name: none;">
											<div class="feature-icon">
												<img src="{{asset('frontend/assets/img/barcode.png')}}" alt="">
											</div>
											<div class="feature-text">
												<h3>Barcode QR Code Reader </h3>
												<p>The user can identify products that contain barcodes within the
													predefined database according to the user's country</p>
											</div>
										</div>
									</div>
									<div class="owl-item  ">
										<div class="single-feature wow fadeInLeft" data-wow-duration="2s"
											data-wow-delay="0.3s"
											style="visibility: hidden; animation-duration: 2s; animation-delay: 0.3s; animation-name: none;">
											<div class="feature-icon">
												<img src="{{asset('frontend/assets/img/icon.png')}}" alt="">
											</div>
											<div class="feature-text">
												<h3>System language settings</h3>
												<p>Enables the user to choose the preferred operating system language as
													well as change the reader speed or choose the default reading speed
												</p>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<a href="{{route('front.applicationpage')}}" class="amal-btn">View All<span></span></a>
			</div>
		</div>
	
	</section>
	
	<!-- Applications Area End -->

<!-- Video Area Start -->
<section class="amal-video-area">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="video-box">
					<div class="video-inn">
						<a class="popup-youtube" href="https://www.youtube.com/watch?v=5Qzxy5b4tXE">
							<i class="flaticon-play"></i>
						</a>
						<h2>Best Product For You. Check The Demo Video.</h2>
						<span class="heading_overlay"></span>
						<!--<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat-->
						<!--    mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper-->
						<!--    suscipit, posuere a, pede.</p>-->
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- Video Area End -->



<!-- About Area Start -->
<section class="amal-about-area" data-scroll-index="2">
	<div class="container">
		<div class="row">
			<div class="col-lg-6">
				<div class="about-left  wow fadeInLeft" data-wow-delay="0.5s"
					style="visibility: hidden; animation-delay: 0.5s; animation-name: none;">
					<img src="{{asset('frontend/assets/img/about.png')}}" alt="about" class="animation-jump">
				</div>
			</div>
			<div class="col-lg-6">
				<div class="about-right  wow fadeInRight" data-wow-delay="0.5"
					style="visibility: hidden; animation-name: none;">
					<div class="about-heading">
						<h4>about product</h4>
						<h2>Awesome Digital Product Can Make Your Life Easier</h2>
						<span class="heading_overlay"></span>
					</div>
					<p>The idea started with the creation of an intelligent robot that can identify the environment
						and movement, which was the field of study and specialization of Engineer Mohammed Islam.
					</p>
					<p>Then the idea turned from spending effort and time in making the robot to making this robot
						serve humanity and harness all its capabilities to be a support for the human being. After
						many research and development, work began to transform this robot since 2010 into smart
						glasses for the blind.
					</p>
					<p>According to the reports of the World Health Organization, the number of blind people has
						reached more than 60 million, and the number of visually impaired people has reached 250
						million. Hence the idea came to invent a smart technology that enables them to coexist
						naturally and to be self-reliant and not to ask for help from others.
					</p>

					<a href="{{route('front.aboutus')}}" class="amal-btn">Read More<span></span></a>

				</div>
			</div>
		</div>
	</div>
</section>
<!-- About Area End -->



<!-- Choose Area Start -->
<section class="amal-choose-area" data-scroll-index="3">
	<div class="container">
		<div class="row">
			<div class="col-lg-6">
				<div class="choose-left">
					<div class="choose-heading wow fadeInLeft" data-wow-duration="2s" data-wow-delay="0.3s"
						style="visibility: hidden; animation-duration: 2s; animation-delay: 0.3s; animation-name: none;">
						<h4>Why Choose us</h4>
						<h2>We offer the best quality smart glasses.</h2>
						<span class="heading_overlay"></span>
					</div>
					<ul>
						<li class="choose-box wow fadeInLeft" data-wow-duration="2s" data-wow-delay="0.5s"
							style="visibility: hidden; animation-duration: 2s; animation-delay: 0.5s; animation-name: none;">
							<div class="choose-icon">
								<img src="{{asset('frontend/assets/img/icon.png')}}" alt="">
							</div>
							<div class="choose-text">
								<h3>High quality</h3>
								<p>Amal glass is made of the best electronic components and micro-sensors, in
									addition to their shatter-resistant lenses and outer frame </p>
							</div>
						</li>
						<li class="choose-box wow fadeInLeft" data-wow-duration="2s" data-wow-delay="0.7s"
							style="visibility: hidden; animation-duration: 2s; animation-delay: 0.7s; animation-name: none;">
							<div class="choose-icon">
								<img src="{{asset('frontend/assets/img/icon.png')}}" alt="">
							</div>
							<div class="choose-text">
								<h3>Excellent customer service</h3>
								<p>We receive your inquiries and comments in Arabic and English throughout the day
									via voice call, WhatsApp messages, or social platforms</p>
							</div>
						</li>
						<li class="choose-box wow fadeInLeft" data-wow-duration="2s" data-wow-delay="0.9s"
							style="visibility: hidden; animation-duration: 2s; animation-delay: 0.9s; animation-name: none;">
							<div class="choose-icon">
								<img src="{{asset('frontend/assets/img/icon.png')}}" alt="">
							</div>
							<div class="choose-text">
								<h3>Best price</h3>
								<p>Amal glass is made of the latest technology, with high quality and at a special
									price</p>
							</div>
						</li>
					</ul>
				</div>
			</div>
			<div class="col-lg-6">
				<div class="choose-right wow fadeInRight" data-wow-duration="2s" data-wow-delay="0.5s"
					style="visibility: hidden; animation-duration: 2s; animation-delay: 0.5s; animation-name: none;">
					<div class="choose-slider owl-carousel owl-loaded owl-drag">



						<div class="owl-stage-outer">
							<div class="owl-stage"
								style="transform: translate3d(-1080px, 0px, 0px); transition: all 0s ease 0s; width: 3780px;">
								<div class="owl-item cloned" style="width: 540px;">
									<div class="single-choose-item">
										<img src="{{asset('frontend/assets/img/product-3.png')}}" alt="product">
									</div>
								</div>
								<div class="owl-item cloned" style="width: 540px;">
									<div class="single-choose-item">
										<img src="{{asset('frontend/assets/img/product-4.png')}}" alt="product">
									</div>
								</div>
								<div class="owl-item active" style="width: 540px;">
									<div class="single-choose-item">
										<img src="{{asset('frontend/assets/img/product-1.png')}}" alt="product">
									</div>
								</div>
								<div class="owl-item" style="width: 540px;">
									<div class="single-choose-item">
										<img src="{{asset('frontend/assets/img/product-3.png')}}" alt="product">
									</div>
								</div>
								<div class="owl-item" style="width: 540px;">
									<div class="single-choose-item">
										<img src="{{asset('frontend/assets/img/product-4.png')}}" alt="product">
									</div>
								</div>
								<div class="owl-item cloned" style="width: 540px;">
									<div class="single-choose-item">
										<img src="{{asset('frontend/assets/img/product-1.png')}}" alt="product">
									</div>
								</div>
								<div class="owl-item cloned" style="width: 540px;">
									<div class="single-choose-item">
										<img src="{{asset('frontend/assets/img/product-3.png')}}" alt="product">
									</div>
								</div>
							</div>
						</div>

						<div class="owl-dots disabled"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- Choose Area End -->


<!-- Testimonial Area Start -->
<section class="amal-testimonial-area" id="testimonial" data-scroll-index="4">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="site-heading wow fadeInUp" data-wow-duration="2s" data-wow-delay="0.3s"
					style="visibility: hidden; animation-duration: 2s; animation-delay: 0.3s; animation-name: none;">
					<h4>Trophies</h4>
					<h2>AmalGlass Trophies</h2>
					<span class="heading_overlay"></span>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="testimonial-slider owl-carousel wow fadeInUp owl-loaded owl-drag" data-wow-duration="2s"
					data-wow-delay="0.5s"
					style="visibility: hidden; animation-duration: 2s; animation-delay: 0.5s; animation-name: none;">

					<div class="owl-stage-outer">
						<div class="owl-stage"
							style="transform: translate3d(-1506px, 0px, 0px); transition: all 0s ease 0s; width: 5651px;">
							<div class="owl-item " style="width: 356.667px; margin-right: 20px;">
								<div class="single-testimonial">
									<div class="testimonial-text">
										<p>Third Place 2019</p>
										<span><i class="fa fa-quote-right"></i></span>
									</div>
									<div class="testimonial-image">
										<img src="{{asset('frontend/assets/img/awards1.jpeg')}}" alt="" class="img-fluid">
									</div>
									<div class="testimonial-meta">
										<h4 class="client-title">Artificial intelligence competition</h4>
										<p class="client-company">Dubai - United Arab Emirates</p>
									</div>
								</div>
							</div>
							<div class="owl-item " style="width: 356.667px; margin-right: 20px;">
								<div class="single-testimonial">
									<div class="testimonial-text">
										<p>First Place 2014</p>
										<span><i class="fa fa-quote-right"></i></span>
									</div>
									<div class="testimonial-image">
										<img src="{{asset('frontend/assets/img/awards2.jpeg')}}" alt="" class="img-fluid">
									</div>
									<div class="testimonial-meta">
										<h4 class="client-title">Arab Innovation Network</h4>
										<p class="client-company">Abu Dhabi - United Arab Emirates</p>
									</div>
								</div>
							</div>
							<div class="owl-item  " style="width: 356.667px; margin-right: 20px;">
								<div class="single-testimonial">
									<div class="testimonial-text">
										<p>United Arab Emirates First place 2019</p>
										<span><i class="fa fa-quote-right"></i></span>
									</div>
									<div class="testimonial-image">
										<img src="{{asset('frontend/assets/img/awards3.jpeg')}}" alt="" class="img-fluid">
									</div>
									<div class="testimonial-meta">
										<h4 class="client-title">Entrepreneurship world cup</h4>
										<p class="client-company">Riyadh - Saudi Arabia</p>
									</div>
								</div>
							</div>
							<div class="owl-item  " style="width: 356.667px; margin-right: 20px;">
								<div class="single-testimonial">
									<div class="testimonial-text">
										<p>Top 100 start up shaping the 4th industrial revolution in the middle east
										</p>
										<span><i class="fa fa-quote-right"></i></span>
									</div>
									<div class="testimonial-image">
										<img src="{{asset('frontend/assets/img/awards4.jpeg')}}" alt="" class="img-fluid">
									</div>
									<div class="testimonial-meta">
										<h4 class="client-title">World Economic Form</h4>
										<p class="client-company">Amman - Jordan</p>
									</div>
								</div>
							</div>


						</div>
					</div>

				</div>
			</div>
		</div>
	</div>
</section>
<!-- Testimonial Area End -->



<!-- Shop Area Start -->
<section class="amal-product-area" id="product" data-scroll-index="5">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="site-heading wow fadeInUp" data-wow-duration="2s" data-wow-delay="0.3s"
					style="visibility: hidden; animation-duration: 2s; animation-delay: 0.3s; animation-name: none;">
					<h4>Store</h4>
					<h2>Amal Glass Online Shop</h2>
					<span class="heading_overlay"></span>
				</div>

				<div class="row">
					<div class="col-xs-12 col-lg-10 " style=" margin: auto;">
						<nav>
							<div class="nav nav-tabs nav-fill fix" id="nav-tab" role="tablist">
								<a class="nav-item nav-link uae active" id="nav-UAE-tab" data-toggle="tab"
									href="#nav-UAE" role="tab" aria-controls="nav-UAE" aria-selected="true">UAE
									Store</a>
								<a class="nav-item nav-link ksa" id="nav-KSA-tab" data-toggle="tab" href="#nav-KSA"
									role="tab" aria-controls="nav-KSA" aria-selected="false">KSA Store</a>
								<a class="nav-item nav-link int" id="nav-International-tab" data-toggle="tab"
									href="#nav-International" role="tab" aria-controls="nav-International"
									aria-selected="false"> International Store</a>

							</div>
						</nav>
					</div>
					<div class="col-xs-12 ">
						<div class="tab-content py-3 px-3 px-sm-0" id="nav-tabContent">
							<div class="tab-pane fade show active" id="nav-UAE" role="tabpanel"
								aria-labelledby="nav-UAE-tab">
								<div class="row box">
									<form>
										<div class="row item">
											<div class="col-lg-4 col-md-6">

												<div class="product-item ">

													<div class="product-text">
														<img src="{{asset('frontend/assets/img/icon.png')}}" alt="product">
														<h3><a href="#">UAE Store</a></h3>
														<div class="product-meta">
															<p>FREE SHIPPING</p>
															<p>12 Months Subscription for Paid Apps</p>
															<p> &nbsp</p>
														</div>

														<a href="#"
															data-href="https://buy.stripe.com/cN2eVB38S47x8xO5kk"
															class="amal-btn btn-buy">4,000 AED</span></a>
													</div>
												</div>

											</div>
											<div class="col-lg-4 col-md-6">
												<div class="product-item ">

													<div class="product-text">
														<img src="{{asset('frontend/assets/img/icon.png')}}" alt="product">
														<h3><a href="#">UAE Store</a></h3>
														<div class="product-meta">
															<p>FREE SHIPPING</p>
															<p>12 Months Subscription for Paid Apps</p>
															<p>Samsung Mobile</p>
														</div>

														<a href="#"
															data-href="https://buy.stripe.com/5kAaFl7p847xdS8eUZ"
															class="amal-btn btn-buy">5,000 AED</span></a>
													</div>
												</div>
											</div>
											<div class="col-lg-4 col-md-6">
												<div class="product-item ">

													<div class="product-text">
														<img src="{{asset('frontend/assets/img/icon.png')}}" alt="product">
														<h3><a href="#">UAE Store</a></h3>
														<div class="product-meta">
															<p> <span
																	style="font-weight: bold;font-size: 18px; font-style: italic;">12
																	months</span> <br> subscription for paid apps
															</p>
															<p> &nbsp </p>
															<p> &nbsp </p>
														</div>

														<a href="#"
															data-href="https://buy.stripe.com/cN2aFlbFobzZcO48wA"
															class="amal-btn btn-buy">200 AED</span></a>
													</div>
												</div>
											</div>
										</div>
										<div class="form-group form-check" style=" margin: 0 24px;">
											<input type="checkbox" class="form-check-input" id="exampleCheck1">
											<label class="form-check-label" for="exampleCheck1">Proceeding to
												purchase confirms reading and
												accepting <span><a href="Privact_policy.html">terms & conditions and
														Privacy</a></span></label>
										</div>
										<!-- <button type="submit" class="amal-btn">Buy Now</button> -->
									</form>

								</div>
							</div>
							<div class="tab-pane fade" id="nav-KSA" role="tabpanel" aria-labelledby="nav-KSA-tab">
								<div class="row box">
									<form>
										<div class="row item">
											<div class="col-lg-4 col-md-6">

												<div class="product-item ">

													<div class="product-text">
														<img src="{{asset('frontend/assets/img/icon.png')}}" alt="product">
														<h3><a href="#">KSA Store</a></h3>
														<div class="product-meta">
															<p>FREE SHIPPING</p>
															<p>12 Months Subscription for Paid Apps</p>
															<p> &nbsp</p>
														</div>

														<a href="#"
															data-href="https://buy.stripe.com/6oE28P9xggUjcO48wC"
															class="amal-btn btn-buy"> 4,750 SAR</span></a>
													</div>
												</div>

											</div>
											<div class="col-lg-4 col-md-6">
												<div class="product-item ">

													<div class="product-text">
														<img src="{{asset('frontend/assets/img/icon.png')}}" alt="product">
														<h3><a href="#">KSA Store</a></h3>
														<div class="product-meta">
															<p>FREE SHIPPING</p>
															<p>12 Months Subscription for Paid Apps</p>
															<p>Samsung Mobile</p>
														</div>

														<a href="#"
															data-href="https://buy.stripe.com/28o6p5cJs6fF4hydQX"
															class="amal-btn btn-buy">5,750 SAR</span></a>
													</div>
												</div>
											</div>
											<div class="col-lg-4 col-md-6">
												<div class="product-item ">

													<div class="product-text">
														<img src="{{asset('frontend/assets/img/icon.png')}}" alt="product">
														<h3><a href="#">KSA Store</a></h3>
														<div class="product-meta">
															<p> <span
																	style="font-weight: bold;font-size: 18px; font-style: italic;">12
																	months</span> <br> subscription for paid apps
															</p>
															<p> &nbsp </p>
															<p> &nbsp </p>
														</div>

														<a href="#"
															data-href="https://buy.stripe.com/fZe28P4cWavV6pGcMU"
															class="amal-btn btn-buy">200 SAR</span></a>
													</div>
												</div>
											</div>
										</div>
										<div class="form-group form-check">
											<input type="checkbox" class="form-check-input" id="exampleCheck1">
											<label class="form-check-label" for="exampleCheck1">Proceeding to
												purchase confirms reading and
												accepting <span><a href="#">Terms of Use and privacy
														Policy</a></span></label>
										</div>
										<!-- <button type="submit" class="amal-btn">Buy Now</button> -->
									</form>

								</div>
							</div>
							<div class="tab-pane fade" id="nav-International" role="tabpanel"
								aria-labelledby="nav-International-tab">
								<div class="row box">
									<form>
										<div class="row item">
											<div class="col-lg-4 col-md-6">

												<div class="product-item ">

													<div class="product-text">
														<img src="{{asset('frontend/assets/img/icon.png')}}" alt="product">
														<h3><a href="#">International Store</a></h3>
														<div class="product-meta">
															<p>FREE SHIPPING</p>
															<p>12 Months Subscription for Paid Apps</p>
															<p> &nbsp</p>
														</div>

														<a href="#"
															data-href="https://buy.stripe.com/eVa28P5h047x01ibIR"
															class="amal-btn btn-buy">1,100 $</span></a>
													</div>
												</div>

											</div>
											<div class="col-lg-4 col-md-6">
												<div class="product-item ">

													<div class="product-text">
														<img src="{{asset('frontend/assets/img/icon.png')}}" alt="product">
														<h3><a href="#">International Store</a></h3>
														<div class="product-meta">
															<p>FREE SHIPPING</p>
															<p>12 Months Subscription for Paid Apps</p>
															<p>Samsung Mobile</p>
														</div>

														<a href="#"
															data-href="https://buy.stripe.com/4gweVBcJseMb4hyfZ8"
															class="amal-btn btn-buy">1,400 $</span></a>
													</div>
												</div>
											</div>
											<div class="col-lg-4 col-md-6">
												<div class="product-item ">

													<div class="product-text">
														<img src="{{asset('frontend/assets/img/icon.png')}}" alt="product">
														<h3><a href="#">International Store</a></h3>
														<div class="product-meta">
															<p> <span
																	style="font-weight: bold;font-size: 18px; font-style: italic;">12
																	months</span> <br> subscription for paid apps
															</p>
															<p> &nbsp </p>
															<p> &nbsp </p>
														</div>

														<a href="#"
															data-href="https://buy.stripe.com/5kA7t9bFogUjbK028j"
															class="amal-btn btn-buy">60 $</span></a>
													</div>
												</div>
											</div>
										</div>
										<div class="form-group form-check">
											<input type="checkbox" class="form-check-input" id="exampleCheck1">
											<label class="form-check-label" for="exampleCheck1">Proceeding to
												purchase confirms reading and
												accepting <span><a href="#">Terms of Use & Privacy
														Policy</a></span></label>
										</div>
										<!-- <button type="submit" class="amal-btn">Buy Now</button> -->
									</form>

								</div>
							</div>

						</div>

					</div>
				</div>


			</div>
		</div>








	</div>
	</div>
	</div>
	</div>
</section>
<!-- Product Area End -->




<!-- Contact Area Start -->
<section class="amal-contact-area" id="contact" data-scroll-index="6">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="site-heading wow fadeInUp" data-wow-duration="2s" data-wow-delay="0.3s"
					style="visibility: hidden; animation-duration: 2s; animation-delay: 0.3s; animation-name: none;">
					<h4>Contact Us</h4>
					<h2>Lets Get In Touch</h2>
					<span class="heading_overlay"></span>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-7">
				<div class="contact-form wow fadeInLeft" data-wow-duration="2s" data-wow-delay="0.5s"
					style="visibility: hidden; animation-duration: 2s; animation-delay: 0.5s; animation-name: none;">
					<form
						action='https://forms.zohopublic.com/mohammed21/form/AmalGlassEnglish/formperma/87ml0wqmD1VCvf9bsMgJ5PGUWBnxSJtUt0EHPVEKfY0/htmlRecords/submit'
						name='form' method='POST'
						onSubmit='javascript:document.charset="UTF-8"; return zf_ValidateAndSubmit();'
						accept-charset='UTF-8' enctype='multipart/form-data' id='form'>
						<div class="row">
							<div class="col-12 col-md-6">
								<p>
									<input required name="Name_First" class="form-control contact-data" type="text"
										placeholder="First Name">
								</p>
							</div>
							<div class="col-12 col-md-6">
								<p>
									<input required name="Name_Last" class="form-control contact-data" type="text"
										placeholder="Last Name">
								</p>
							</div>
							<!--                                <div class="col-12 col-md-6">-->
							<!--                                    <p>-->
							<!--                                        <input  class="form-control" type="text" placeholder="Subject">-->
							<!--                                    </p>-->
							<!--                                </div>-->
						</div>
						<div class="row">
							<div class="col-lg-6">
								<p>
									<input required class="form-control contact-data" type="email" placeholder="E-mail">
								</p>
							</div>
							<div class="col-lg-6">
								<p style="display: inline-flex">
									<input required style="width: 25%" name="PhoneNumber_countrycodeval"
										class="form-control contact-data" pattern="^[+]\d*" type="tel"
										placeholder="Code" value="+">
									<input required style="width: 75%" name="PhoneNumber_countrycode"
										class="form-control contact-data" type="tel" pattern="\d*" placeholder="Phone">
								</p>
							</div>
						</div>
						<div class="row">
							<div class="col-lg-12">
								<p>
									<textarea required class="form-control contact-data"
										placeholder="Your Message..."></textarea>
								</p>
							</div>
						</div>
						<div class="row">
							<div class="col-lg-12">
								<p>
									<button type="submit" class="amal-btn">Send Message <span></span></button>
								</p>
							</div>
						</div>
					</form>

				</div>
			</div>
			<div class="col-lg-5">
				<div class="contact-info">
					<div class="single-contact-info wow fadeInUp" data-wow-duration="2s" data-wow-delay="0.3s"
						style="visibility: hidden; animation-duration: 2s; animation-delay: 0.3s; animation-name: none;">
						<div class="">
							<img src="{{asset('frontend/assets/img/icon-white.png')}}" alt="" style="max-width: none;">
						</div>
						<div class="contact-info-text">
							<h3>Our head office</h3>
							<p>Office 2603,Liwa Heights,Cluster W
								JLT , Dubai , United Arab Emirates</p>
						</div>
					</div>
					<div class="single-contact-info wow fadeInUp" data-wow-duration="2s" data-wow-delay="0.5s"
						style="visibility: hidden; animation-duration: 2s; animation-delay: 0.5s; animation-name: none;">
						<div class="">
							<img src="{{asset('frontend/assets/img/icon-white.png')}}" alt="">
						</div>
						<div class="contact-info-text">
							<h3>Call Us On</h3>
							<p>0097144237754</p>
						</div>
					</div>
					<div class="single-contact-info wow fadeInUp" data-wow-duration="2s" data-wow-delay="0.7s"
						style="visibility: hidden; animation-duration: 2s; animation-delay: 0.7s; animation-name: none;">
						<div class="">
							<img src="{{asset('frontend/assets/img/icon-white.png')}}" alt="">
						</div>
						<div class="contact-info-text">
							<h3>Email Us At</h3>
							<p>Info@AmalGlass.com</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- Contact Area End -->

@endsection

@section('scripts')



