<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class FrontendController extends Controller
{
    //
    public function WelcomePage(){
        return view('frontend.welcome');

    }

    public function MediaPage()
    {
        return view('frontend.media');
    }

    public function aboutUs(){
        return view('frontend.about');
    }

    public function applicationPage(){
        return view('frontend.applications');
    }
}
