<?php

use App\Http\Controllers\Frontend\FrontendController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('cms.index');
});
// Route::get('/', function () {
//     return view('frontend.application');
// });
//Front  end  Controllers 
Route::get('/welcomepage' , [FrontendController::class , 'WelcomePage'])->name('front.welcome');
Route::get('/mediapage', [FrontendController::class, 'MediaPage'])->name('front.media');
Route::get('/aboutuspage' , [FrontendController::class, 'aboutUs'])->name('front.aboutus');
Route::get('/applicationpage', [FrontendController::class, 'applicationPage'])->name('front.applicationpage');

